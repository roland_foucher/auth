DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`(  
    id int PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
) DEFAULT CHARSET UTF8 COMMENT '';