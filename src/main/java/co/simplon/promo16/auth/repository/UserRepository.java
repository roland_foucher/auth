package co.simplon.promo16.auth.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.auth.entity.User;

@Repository
public class UserRepository {
    @Autowired
    private DataSource dataSource;

    public boolean save(User user) {
        try {

            PreparedStatement stmt = dataSource.getConnection()
            .prepareStatement("INSERT INTO user (email,password,role) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getRole());
            //cette partie sert à récupérer la primary key auto incrémenté et à l'assigner
            // à l'instance de User qu'on vient de faire persister
            if(stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                user.setId(result.getInt(1));

                return true;
            }


        } catch (SQLException e) {
            
            e.printStackTrace();
        }

        return false;
    }

    public User findByEmail(String email) {
        try {
            PreparedStatement stmt = dataSource.getConnection().prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("id"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getString("role"));
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }


}
